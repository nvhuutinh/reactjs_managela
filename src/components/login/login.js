import React from 'react'
import { Link } from 'react-router-dom'

export default class Login extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      password: '',
    }
    this.onSubmit = this.onSubmit.bind(this)
  }
  onSubmit(e) {
    e.preventDefault()
    this.props.login(this.state.username, this.state.password)
  }

  render() {
    return (
      <div className="background">
        <div className="login-page animated bounceIn">
          <div className="form">
            <Link to="/">
              <span className="fa fa-close exit" />
            </Link>
            <h2 className="title">Login</h2>
            {this.props.message && <div className="alert alert-danger animated bounceIn">{this.props.message}</div>}

            <form className="login-form" onSubmit={this.onSubmit} style={{ margin: '0 auto' }}>
              <input
                type="text"
                className=""
                name="username"
                placeholder="User Name"
                required
                onChange={e => this.setState({ username: e.target.value })}
                value={this.state.username}
              />
              <input
                type="password"
                className=""
                name="password"
                placeholder="Password"
                required
                onChange={e => this.setState({ password: e.target.value })}
                value={this.state.password}
              />
              <button type="submit">Login </button>
              <p className="message">
                <Link to="/forgot">Forgot Password ?</Link>
              </p>
              <p className="message">
                Not registered? <Link to="register">Create an account</Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
