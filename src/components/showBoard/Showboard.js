import React from 'react'
import PersonalBoard from './PersonalBoard'

class Showboard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      board: [],
    }
  }
  componentWillReceiveProps(nextProps) {
    this.setState({
      board: nextProps.board,
    })
  }
  render() {
    return <PersonalBoard board={this.state.board} createBoard={this.props.createBoard} />
  }
}

export default Showboard
