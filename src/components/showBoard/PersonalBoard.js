import React from 'react'
import Board from './Board'
import NavBar from './NavBar'

class PersonalBoard extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      title: '',
    }
    this.createBoard = this.createBoard.bind(this)
  }
  createBoard(e) {
    e.preventDefault()
    this.props.createBoard(this.state.title)
    this.setState({
      title: '',
    })
  }

  render() {
    return (
      <div>
        <NavBar />
        <div className="showboard">
          <h1>
            <b>List Board</b>
          </h1>
          <div className="row">
            {this.props.board.map((board, i) => (
              <Board
                key={i.toString()}
                className="col-xs-3"
                style={{ padding: 50, margin: 30, height: 100 }}
                title={board.title}
                id={board.id}
              />
            ))}
            <div className="col-xs-3 add-board-item" style={{ backgroundColor: '#E2E4E6' }}>
              <form>
                <input
                  placeholder="Enter board title"
                  type="text"
                  style={{ backgroundColor: 'transparent', border: 'none', height: '100px' }}
                  onChange={e => this.setState({ title: e.target.value })}
                  value={this.state.title}
                />
                <button
                  disabled={!this.state.title}
                  type="submit"
                  onClick={this.createBoard}
                  className="btn glyphicon glyphicon-plus"
                  style={{ backgroundColor: '#E2E4E6', fontSize: 30 }}
                />
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default PersonalBoard
