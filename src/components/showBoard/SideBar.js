import React from 'react'
import Member from './Member'
import { Link } from 'react-router-dom'

function SideBar() {
  return (
    <div style={{ marginTop: -20 }}>
      <nav className="sidebar" style={{ padding: 0, margin: 0 }}>
        <div className="container-fluid" style={{ padding: 0, margin: 0 }}>
          <div className="navbar-header">
            <Link style={{ marginLeft: -30 }} className="navbar-brand" to="">
              My Team<span style={{ fontSize: 20, paddingLeft: 10 }} className="glyphicon glyphicon-plus-sign" />
            </Link>
          </div>
          <div className="collapse navbar-collapse" id="bs-sidebar-navbar-collapse-1">
            <ul className="nav navbar-nav" style={{ margin: 'auto' }}>
              <Member name="Kim Jisoo" img="https://i.pinimg.com/736x/47/af/4f/47af4f843a91ec1356e5d47541ad3542.jpg" />
              <Member
                name="Kim Jennie"
                img="https://78.media.tumblr.com/676e8c6ccadeb7d8cd9f9419b34765b7/tumblr_osh7xguU351ugjrm3o4_500.jpg"
              />
              <Member
                name="Gal Gadot"
                img="http://www.filmstaroutfits.com/image/cache/catalog/Womens%20Jackets/Fast-And-Furious-6-Gal-Gadot-Leather-Jacket-2-500x500.jpg"
              />
              <Member
                name="Sandara Park"
                img="http://images6.fanpop.com/image/photos/33600000/Dara-sandara-park-33626586-500-500.png"
              />
            </ul>
          </div>
        </div>
      </nav>
    </div>
  )
}

export default SideBar
