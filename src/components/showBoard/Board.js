import React from 'react'
import { Link } from 'react-router-dom'

function Board({ title, id }) {
  const url = 'detail/' + id
  return (
    <Link to={url} style={{ textDecoration: 'none' }}>
      <div className="card board-item" style={{ backgroundColor: '#4CAF50' }}>
        <p style={{ color: 'white', fontSize: 30, padding: '25px 0', fontFamily: 'Roboto' }}>{title}</p>
      </div>
    </Link>
  )
}

export default Board
