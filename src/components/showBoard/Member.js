import React from 'react'
import { Link } from 'react-router-dom'

function Member({ name, img }) {
  return (
    <div className="member" style={{ paddingBottom: 5, paddingTop: 5 }}>
      <li>
        <Link to="#">
          <img
            style={{ height: 40, paddingRight: 5, paddingLeft: 15, color: '#00000' }}
            src={img}
            className="img-circle"
            alt="Avatar"
          />
          {name}
        </Link>
      </li>
    </div>
  )
}

export default Member
