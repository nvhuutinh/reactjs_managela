import React from 'react'
import { Link } from 'react-router-dom'
function NavBar() {
  return (
    <div>
      <div className="w3-bar w3-light-grey">
        <a href="/" className="w3-bar-item w3-tangerine w3-xxlarge w3-text-green">
          <b>Managela</b>
        </a>

        <form className="w3-bar-item w3-display-topmiddle form-inline">
          <div className="input-group add-on">
            <input className="form-control" placeholder="Search" name="srch-term" id="srch-term" type="text" />
            <div className="input-group-btn">
              <button className="btn btn-default" type="submit">
                <i className="fa fa-search w3-text-green" />
              </button>
            </div>
          </div>
        </form>
        <div className="w3-right w3-bar-item " />

        <Link
          to="/"
          className="w3-bar-item w3-button w3-right w3-xlarge w3-text-green"
          onClick={e => {
            localStorage.removeItem('token')
            localStorage.removeItem('username')
          }}
        >
          <i className="fa fa-sign-in" />
        </Link>

        <Link to="" className="w3-bar-item w3-button w3-right w3-text-green w3-xlarge">
          <i className="fa fa-globe" />
        </Link>
        <p className="w3-bar-item w3-right " />
      </div>
    </div>
  )
}

export default NavBar
