import React from 'react'
import { Link } from 'react-router-dom'

class HomeNavBar extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-expand-sm">
        <div>
          <ul className="navbar-nav" style={{ position: 'absolute', left: 0 }}>
            <Link to="/" className="nav-link btn btn-success home-btn">
              Home
            </Link>
          </ul>
          <ul className="navbar-nav" style={{ position: 'absolute', right: 0 }}>
            <Link to="/login" className="nav-link btn btn-success home-btn">
              Login
            </Link>
            <Link to="/register" className="nav-link btn btn-success home-btn">
              Register
            </Link>
          </ul>
        </div>
      </nav>
    )
  }
}

export default HomeNavBar
