import React from 'react'
import { Link } from 'react-router-dom'

class Homepage extends React.Component {
  componentWillMount() {
    if (localStorage.getItem('token')) this.props.history.push('/board')
  }
  render() {
    return (
      <div className="background">
        <div className="center">
          <h2 className="animated bounceInLeft">Managela lets you follow what are you doing</h2>
          <p className="animated bounceInRight">
            With Managela's board, list, ticket enable you to organize and prioritize your projects flexible and
            rewarding way
          </p>
          <div>
            <Link to="/register">
              <button className="btn btn-success join-btn animated bounceInUp">
                <b style={{ fontSize: 30 }}>GET STARTED</b>
              </button>
            </Link>
          </div>
          <div className="text animated bounceInUp" style={{ marginTop: 20 }}>
            Already have account?{' '}
            <Link className="btn btn-success" style={{ fontSize: 20 }} to="/login">
              {' '}
              Login{' '}
            </Link>{' '}
            here!
          </div>
        </div>
      </div>
    )
  }
}

export default Homepage
