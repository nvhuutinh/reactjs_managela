import React from 'react'

export default class Forgot extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
    }
    this.onSubmit = this.onSubmit.bind(this)
    console.log(this.props)
  }

  onSubmit(e) {
    e.preventDefault()
    this.props.forgot(this.state.email)
  }
  render() {
    return (
      <div className="background">
        <div className="login-page animated bounceIn">
          <div className="form">
            <h2 className="title">Forgot</h2>
            {this.props.message && <p className="alert alert-success">{this.props.message}</p>}

            <form className="login-form" onSubmit={this.onSubmit} style={{ margin: '0 auto' }}>
              <input
                type="text"
                className=""
                name="email"
                placeholder="Email Address"
                required
                onChange={e => this.setState({ email: e.target.value })}
                value={this.state.email}
              />
              <button className="" type="submit" value="Forgot">
                Forgot
              </button>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
