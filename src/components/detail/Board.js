import React from 'react'

import styled from 'styled-components'
import { Droppable, DragDropContext } from 'react-beautiful-dnd'

import List from './List'
const Container = styled.div`
  min-width: 100vw;
  display: inline-flex;
  overflow: scroll;
  height: 100%;
`

const getBoardStyle = isDraggingOver => ({
  display: 'inline-flex',
})

class Board extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lanes: props.lanes,
      laneName: '',
    }
    this.onDragEnd = this.onDragEnd.bind(this)
  }
  moveCard(cardId, cardIndex, sourceId, desId, desIndex) {
    let temp = this.state.lanes[this.indexFromId(this.state.lanes, sourceId)].cards[cardIndex]
    let board = this.state.lanes
    board[this.indexFromId(board, sourceId)].cards.splice(cardIndex, 1)
    board[this.indexFromId(board, desId)].cards.splice(desIndex, 0, temp)
    this.setState({
      lanes: board,
    })
  }
  moveList(listId, listIndex, destination) {
    let temp = this.state.lanes[listIndex]
    let board = this.state.lanes
    board.splice(listIndex, 1)
    board.splice(destination, 0, temp)
    this.setState({
      lanes: board,
    })
  }
  indexFromId(array, id) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) return i
    }
    return -1
  }
  onDragEnd(result) {
    switch (result.type) {
      case 'CARD':
        if (result.destination)
          this.moveCard(
            result.draggableId,
            result.source.index,
            result.source.droppableId,
            result.destination.droppableId,
            result.destination.index
          )
        break
      case 'LIST':
        if (result.destination) this.moveList(result.draggableId, result.source.index, result.destination.index)
        break
      default:
        return
    }
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      lanes: nextProps.lanes,
    })
  }

  render() {
    return (
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Container>
            <Droppable droppableId="droppable" type="LIST" direction="horizontal">
              {(provided, snapshot) => (
                <div ref={provided.innerRef} style={getBoardStyle(snapshot.isDraggingOver)}>
                  {this.state.lanes.map((item, index) => (
                    <List
                      key={item.id}
                      id={item.id}
                      title={item.title}
                      cards={item.cards}
                      onAddCard={this.props.onAddCard}
                      onCardClick={this.props.onCardClick}
                    />
                  ))}
                  {provided.placeholder}
                  <div
                    style={{
                      display: 'block',
                      width: 250,
                      height: 200,
                      background: 'pink',
                      margin: 8,
                      borderRadius: 10,
                      boxShadow: '1px 1px 3px #424242',
                    }}
                  >
                    <form
                      onSubmit={e => {
                        e.preventDefault()
                        this.props.onAddLane(this.props.id, this.state.laneName)
                        this.setState({ laneName: '' })
                      }}
                    >
                      <input
                        style={{ width: '90%', marginTop: 35, height: 57,marginLeft: 10 }}
                        type="text"
                        className="form-control"
                        placeholder="Enter list name"
                        value={this.state.laneName}
                        onChange={e => this.setState({ laneName: e.target.value })}
                      />
                      <button
                        type="submit"
                        disabled={!this.state.laneName}
                        style={{ width: '90%', height: 55, marginTop: 20 }}
                        className="btn"
                      >
                        {' '}
                        <span style={{ fontSize: 30 }} className="glyphicon glyphicon-plus" />{' '}
                      </button>
                    </form>
                  </div>
                </div>
              )}
            </Droppable>
          </Container>
        </DragDropContext>
    )
  }
}

export default Board
