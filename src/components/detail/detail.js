import React from 'react'
import Board from './Board'
import moment from 'moment'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import 'react-datepicker/dist/react-datepicker-cssmodules.css'

import {
  Modal,

  Button,
  FormGroup,
  ControlLabel,
  Form,
  FormControl,
  Col,
  Grid,
  Row,
  ButtonToolbar,
  DropdownButton,
} from 'react-bootstrap'
const customStyles = {
  content : {
    top                   : '50%',
    left                  : '50%',
    right                 : 'auto',
    bottom                : 'auto',
    marginRight           : '-50%',
    transform             : 'translate(-50%, -50%)'
  }
};

export default class Detail extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      lanes: [],
      card:{
        title:''
      },
      showModal: false,
      priority:'' ,
      cardID: '',
      startDate: moment(),
      email: '',
    }
    this.onAddCard = this.onAddCard.bind(this)
    this.onAddLane = this.onAddLane.bind(this)
    this.onCardClick = this.onCardClick.bind(this)
    //cua tinh
    this.closeCardModal = this.closeCardModal.bind(this)
    this.openCardModal = this.openCardModal.bind(this)
    this.addmemberBoard = this.addmemberBoard.bind(this)
    this.handleChange = this.handleChange.bind(this)

   

  }

  handleChange(date) {
    this.setState({ startDate: date })
    this.toggleCalendar()
  }

  toggleCalendar(e) {
    e && e.preventDefault()
    this.setState({ isOpen: !this.state.isOpen })
  }



closeCardModal() {
    this.setState({ showModal: false })
    console.log('123123',this.state.startDate);

    // this.props.addDuedate(this.state.cardID,this.state.startDate);
  }

  openCardModal() {
    this.setState({ showModal: true })
  }
  addmemberBoard(e){
    e.preventDefault()

    this.props.addBoardMember(this.state.email)
  }

  componentWillReceiveProps(nextProps) {
    this.setState({ lanes: nextProps.lanes,card:nextProps.card,startDate:nextProps.card.dueDate})

    console.log('card load component',this.state.card)
    
    
  }
  indexFromId(array, id) {
    for (let i = 0; i < array.length; i++) {
      if (array[i].id === id) return i
    }
    return -1
  }
  onAddCard(laneId, title) {
    this.props.onAddCard(this.indexFromId(this.state.lanes, laneId), laneId, title)
  }
  onAddLane(id, title) {
    this.props.onAddLane(id, title)
  }

  onCardClick(cardId) {
    console.log('Card Id la:', cardId);
    this.setState({cardID:cardId});
    //open car modal 
    
    this.setState({ showModal: true })
    this.props.loadCard(cardId);
    
    console.log('props here',   this.props);
    console.log('onclick state card',this.state.card)
  
  }
  render() {
    return (
      <div style={{ overflow: 'auto', height: '100vh', backgroundColor: '#caefd3' }}>
        <Board 
          id={this.props.id} 
          lanes={this.state.lanes} 
          onAddCard={this.onAddCard} 
          onAddLane={this.onAddLane} 
          onCardClick={this.onCardClick}
        />


        <Modal className="modal-dialog" show={this.state.showModal} onHide={this.closeCardModal}>
          <Modal.Header style={{ display: 'inline' }} closeButton>
            {this.state.card && <h1> {this.state.card.title}</h1>}
            {console.log('------------card in render---------',this.state.card)}
          </Modal.Header>
          <Modal.Body>
            <Grid>
              <Row className="show-grid">
                <Col xs={12} md={8}>
                  <Row className="show-grid" />

                  <Row className="show-grid">
                    <FormGroup controlId="formControlsTextarea">
                      <ControlLabel>Add Comment:</ControlLabel>
                      <FormControl componentClass="textarea" placeholder="Write Something...." />
                      <Button bsSize="small" bsStyle="success">
                        Comment
                      </Button>
                    </FormGroup>
                  </Row>
                </Col>
                <Col xs={6} md={4}>
                  <ButtonToolbar>
                    <DropdownButton title="Member" noCaret id="dropdown-no-caret">
                      <FormGroup controlId="formHorizontalEmail">
                        <ControlLabel> Members:</ControlLabel>
                        <FormControl type="email" placeholder="Email..." />
                      </FormGroup>
                    </DropdownButton>

                    <DropdownButton title="Priority" noCaret id="dropdown-no-caret">
                      <FormGroup controlId="formHorizontalEmail">
                        <ControlLabel> Priority</ControlLabel>
                        <FormControl
                          onChange={e => {
                           this.setState({ priority: e.target.value })
                           this.props.priority({priority: e.target.value })
                          }}
                          value={this.state.priority}
                          componentClass="select"
                          placeholder="select"

                        >
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </FormControl>
                      </FormGroup>
                    </DropdownButton>
                    {this.state.card && <p>Due Date{this.state.card.dueDate}</p>}
                  

                    <DatePicker
                    dateFormat="DD/MM/YYYY"
                    selected={this.state.startDate}
                    onChange={this.handleChange}
                    isClearable={true}
                    placeholderText="Due Date"
                    className="red-border"
                  />
                  </ButtonToolbar>
                </Col>
              </Row>
            </Grid>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.closeCardModal}>Close</Button>
          </Modal.Footer>
        </Modal>






      </div>
    )
  }
}
