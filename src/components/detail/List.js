import { Droppable, Draggable } from 'react-beautiful-dnd'
import React from 'react'
import Card from './Card'

const getListStyle = (isDraggingOver, isDragging) => ({
  background: 'pink',
  padding: 8,
  margin: 8,
  width: 300,
  borderRadius: '10px',
  boxShadow: '1px 1px 3px #424242',
})

const ListCard = (props) => {
  console.log(props);
  if (!props.props.cards) return null
  else {
    const list = Array.from(props.props.cards).map((card, index) => <Card onCardClick={props.props.onCardClick} key={card.id} title={card.title} id={card.id} />)
    return <div>{list}</div>
  }
}

class List extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      cards: props.cards,
      cardName: '',
    }
  }

  render() {
    return (
      <Draggable key={this.props.id} draggableId={this.props.id} type="LIST">
        {(provided, snapshot) => (
          <div>
            <div ref={provided.innerRef} style={provided.draggableStyle} {...provided.dragHandleProps}>
              <Droppable droppableId={this.props.id} type="CARD">
                {(provided, snapshot) => (
                  <div ref={provided.innerRef} style={getListStyle(snapshot.isDraggingOver, snapshot.isDragging)}>
                    <b style={{ fontSize: 22 }}>{this.props.title}</b>
                    <br />
                    <ListCard props = {this.props}/>
                    {provided.placeholder}
                    <div>
                      <form
                        onSubmit={e => {
                          e.preventDefault()
                          this.props.onAddCard(this.props.id, this.state.cardName)
                          this.setState({ cardName: '' })
                        }}
                      >
                        <input
                          type="text"
                          className="form-control"
                          placeholder="Enter card name"
                          value={this.state.cardName}
                          onChange={e => this.setState({ cardName: e.target.value })}
                        />
                        <button type="submit" disabled={!this.state.cardName} className="btn">
                          {' '}
                          <span className="glyphicon glyphicon-plus" />{' '}
                        </button>
                      </form>
                    </div>
                  </div>
                )}
              </Droppable>
            </div>
            {provided.placeholder}
          </div>
        )}
      </Draggable>
    )
  }
}

export default List
