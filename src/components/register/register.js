import React from 'react'
import { Link } from 'react-router-dom'

export default class Register extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      email: '',
      password: '',
      confirmPassword: '',
    }
    this.onSubmit = this.onSubmit.bind(this)
  }
  onSubmit(e) {
    e.preventDefault()
    this.props.register(this.state.username, this.state.email, this.state.password, this.state.confirmPassword)
  }
  render() {
    return (
      <div className="background">
        <div className="login-page animated bounceIn">
          <div className="form">
            <Link to="/" style={{ textDecoration: 'none' }}>
              <span className="fa fa-close exit" />
            </Link>
            <h2 className="title">Register</h2>

            {this.props.errorMessage && (
              <div className="alert alert-danger animated bounceIn">{this.props.errorMessage}</div>
            )}
            {this.props.successMessage && (
              <div className="alert alert-success animated bounceIn">{this.props.successMessage}</div>
            )}

            <form className="login-form" onSubmit={this.onSubmit} style={{ margin: '0 auto' }}>
              <input
                type="text"
                name="username"
                placeholder="Username"
                required
                onChange={e => this.setState({ username: e.target.value })}
                value={this.state.username}
              />
              <input
                type="text"
                name="email"
                placeholder="Email Address"
                required
                onChange={e => this.setState({ email: e.target.value })}
                value={this.state.email}
              />
              <input
                type="password"
                name="password"
                placeholder="Password"
                required
                onChange={e => this.setState({ password: e.target.value })}
                value={this.state.password}
              />
              <input
                type="password"
                name="password"
                placeholder="Please re-enter your password"
                required
                onChange={e => this.setState({ confirmPassword: e.target.value })}
                value={this.state.confirmPassword}
              />
              <button type="submit">Register</button>
              <p className="message">
                Already have account? <Link to="/login">Login here</Link>
              </p>
            </form>
          </div>
        </div>
      </div>
    )
  }
}
