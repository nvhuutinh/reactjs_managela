import React from 'react'

export default class UpdatePassword extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      newPassword: '',
      confirmPassword: '',
    }
    this.onSubmit = this.onSubmit.bind(this)
    console.log(this.props)
  }

  onSubmit(e) {
    e.preventDefault()
    this.props.updatePassword(this.state.newPassword, this.state.confirmPassword)
  }

  render() {
    return (
      <div className="login-page">
        <div className="form">
          <h2 className="title">Update Password</h2>
          {this.props.message && <p className="alert alert-success">{this.props.message}</p>}

          <form className="login-form" onSubmit={this.onSubmit} style={{ margin: '0 auto' }}>
            <input
              type="password"
              className=""
              name="password"
              placeholder="Password"
              required
              onChange={e => this.setState({ newPassword: e.target.value })}
              value={this.state.newPassword}
            />
            <input
              type="password"
              className=""
              name="confirmPassword"
              placeholder="Confirm Password"
              required
              onChange={e => this.setState({ confirmPassword: e.target.value })}
              value={this.state.confirmPassword}
            />
            <button className="" type="submit" value="UpdatePassword">
              Update
            </button>
          </form>
        </div>
      </div>
    )
  }
}
