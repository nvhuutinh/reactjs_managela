import {
  FORGOT_SUCCESS,
  FORGOT_FAILURE,
  FORGOT_REQUEST,
  VERIFYMAIL_REQUEST,
  VERIFYMAIL_SUCCESS,
  VERIFYMAIL_FAILURE,
  UPDATEPASSWORD_REQUEST,
  UPDATEPASSWORD_SUCCESS,
  UPDATEPASSWORD_FAILURE,
} from '../constants/ActionTypes'
import AuthAPI from '../utils/authApi'

//////////// FORGOT PASSWORD ////////////
function requestForgot(email) {
  return {
    type: FORGOT_REQUEST,
    isAuthenticated: false,
    user: {
      email: email,
    },
  }
}

function receiveForgot(message) {
  return {
    type: FORGOT_SUCCESS,
    isAuthenticated: true,
    message,
  }
}

function forgotError(message) {
  return {
    type: FORGOT_FAILURE,
    isAuthenticated: false,
    message,
  }
}

export const forgot = email => dispatch => {
  dispatch(requestForgot(email))
  AuthAPI.forgot(email)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) dispatch(receiveForgot(json.mess))
      else if (json.err) dispatch(forgotError(json.err))
    })
    .catch(err => dispatch(forgotError(err.message)))
}

/////////////////// VERIFY EMAIL /////////////////

function requestVerifyMail(token) {
  return {
    type: VERIFYMAIL_REQUEST,
    isAuthenticated: false,
    user: {
      token: token,
    },
  }
}

function receiveVerifyMail(message) {
  return {
    type: VERIFYMAIL_SUCCESS,
    isAuthenticated: true,
    message,
  }
}

function verifyMailError(message) {
  return {
    type: VERIFYMAIL_FAILURE,
    isAuthenticated: false,
    message,
  }
}

export const verifyMail = access_token => dispatch => {
  dispatch(requestVerifyMail(access_token))
  AuthAPI.verifyMail(access_token)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.token) {
        dispatch(receiveVerifyMail(json.token))
        // this.props.history.push('/updatePassword?access_token=${access_token}');
      } else if (json.message) dispatch(forgotError(json.message))
    })
    .catch(err => dispatch(verifyMailError(err.message)))
}

/////////////// UPDATE PASSWORD //////////////

function requestUpdate(newPassword, confirmPassword, access_token) {
  return {
    type: UPDATEPASSWORD_REQUEST,
    isAuthenticated: false,

    newPassword: newPassword,
    confirmPassword: confirmPassword,
    access_token: access_token,
  }
}

function receiveUpdate(message) {
  return {
    type: UPDATEPASSWORD_SUCCESS,
    isAuthenticated: true,
    message,
  }
}

function updateError(message) {
  return {
    type: UPDATEPASSWORD_FAILURE,
    isAuthenticated: false,
    message,
  }
}
export const updatePassword = (newPassword, confirmPassword, access_token) => dispatch => {
  dispatch(requestUpdate(newPassword, confirmPassword, access_token))
  AuthAPI.updatePassword(newPassword, confirmPassword, access_token)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) {
        dispatch(receiveUpdate(json.mess))
      } else if (json.err) dispatch(updateError(json.err))
    })
    .catch(err => dispatch(updateError(err.message)))
}
