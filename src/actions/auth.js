import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE } from '../constants/ActionTypes'
import AuthAPI from '../utils/authApi'
import { push } from 'react-router-redux'

function requestLogin(username, password) {
  return {
    type: LOGIN_REQUEST,
    isAuthenticated: false,
    user: {
      username: username,
      password: password,
    },
  }
}

function receiveLogin(token) {
  return {
    type: LOGIN_SUCCESS,
    isAuthenticated: true,
    token: token,
  }
}

function loginError(message) {
  return {
    type: LOGIN_FAILURE,
    isAuthenticated: false,
    message,
  }
}

export const login = (username, password) => dispatch => {
  dispatch(requestLogin(username, password))
  AuthAPI.login(username, password)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.user) {
        localStorage.setItem('token', json.user.token)
        localStorage.setItem('username', json.user.username)
        console.log(this.context)
        dispatch(push('/board'))
        dispatch(receiveLogin(json.user.token))
      } else if (json.err) {
        dispatch(loginError(json.err))
      }
    })
    .catch(err => dispatch(loginError(err.message)))
}
