import { PRIORITY_REQUEST,
         PRIORITY_SUCCESS, 
         PRIORITY_FAILURE,

         ADDMEMBERBOARD_REQUEST,
         ADDMEMBERBOARD_SUCCESS ,
         ADDMEMBERBOARD_FAILURE,




          } from '../constants/ActionTypes';
          
import DetailAPI from '../utils/detailApi';
function requestPriority(priority) {
  return {
    type: PRIORITY_REQUEST,
    isSuccess: false,
    card: {
      priority: priority,
    },
  }
}

function receivePriority(message) {
  return {
    type: PRIORITY_SUCCESS,
    isSuccess: true,
    message,
  }
}
function errorPriority(message) {
  return {
    type: PRIORITY_FAILURE,
    isSuccess: false,
    message,
  }
}
export const priority = priority => dispatch => {
  dispatch(receivePriority(priority))
  DetailAPI.priorityAPI(priority)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) dispatch(receivePriority(json.mess))
      else if (json.err) dispatch(errorPriority(json.err))
    })
    .catch(err => dispatch(errorPriority(err.message)))
}



function requestMemberBoard(email) {
  return {
    type: ADDMEMBERBOARD_REQUEST,
    isSuccess: false,
    board: {
      email: email,
    },
  }
}

function receiveMemberBoard(message) {
  return {
    type: ADDMEMBERBOARD_SUCCESS,
    isSuccess: true,
    message,
  }
}
function errorMemberBoard(message) {
  return {
    type: ADDMEMBERBOARD_FAILURE,
    isSuccess: false,
    message,
  }
}


export const addBoardMember = email => dispatch => {
  dispatch(requestMemberBoard(email))
  DetailAPI.addMemberAPI(email)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) dispatch(receiveMemberBoard(json.mess))
      else if (json.err) dispatch(errorMemberBoard(json.err))
    })
    .catch(err => dispatch(errorMemberBoard(err.message)))
}

