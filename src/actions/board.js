import {
  CREATEBOARD_SUCCESS,
  CREATEBOARD_FAILURE,
  CREATEBOARD_REQUEST,
  LOADBOARD_REQUEST,
  LOADBOARD_SUCCESS,
  LOADBOARD_FAILURE,
  LOADDETAIL_REQUEST,
  LOADDETAIL_SUCCESS,
  LOADDETAIL_FAILURE,
  CREATELIST_REQUEST,
  CREATELIST_FAILURE,
  CREATELIST_SUCCESS,
  CREATECARD_REQUEST,
  CREATECARD_FAILURE,
  CREATECARD_SUCCESS,

  ADDDUEDATE_REQUEST,
  ADDDUEDATE_SUCCESS,
  ADDDUEDATE_FAILURE,
  

  PRIORITY_REQUEST,
  PRIORITY_SUCCESS, 
  PRIORITY_FAILURE,

  ADDMEMBERBOARD_REQUEST,
  ADDMEMBERBOARD_SUCCESS ,
  ADDMEMBERBOARD_FAILURE,

  LOADCARD_REQUEST,
  LOADCARD_SUCCESS,
  LOADCARD_FAILURE,

} from '../constants/ActionTypes'
import boardApi from '../utils/boardApi'
import DetailAPI from '../utils/detailApi'

//////////// CREATE BOARD ////////////
function requestCreateboard(title) {
  return {
    type: CREATEBOARD_REQUEST,
    isSuccess: false,
    board: {
      title: title,
    },
  }
}

function receiveCreateboard(board) {
  return {
    type: CREATEBOARD_SUCCESS,
    isSuccess: true,
    board,
  }
}

function createboardError(message) {
  return {
    type: CREATEBOARD_FAILURE,
    isSuccess: false,
    message,
  }
}

export const createBoard = title => dispatch => {
  dispatch(requestCreateboard(title))
  boardApi
    .createBoard(title)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.board) {
        dispatch(receiveCreateboard(json.board))
      } else if (json.err) dispatch(createboardError(json.err))
    })
    .catch(err => dispatch(createboardError(err.message)))
}

//////////// LOAD BOARD ////////////
function requestLoadboard() {
  return {
    type: LOADBOARD_REQUEST,
    isSuccess: false,
  }
}

function receiveLoadboard(board) {
  return {
    type: LOADBOARD_SUCCESS,
    isSuccess: true,
    board,
  }
}

function loadboardError(message) {
  return {
    type: LOADBOARD_FAILURE,
    isSuccess: false,
    message,
  }
}

export const loadBoard = () => dispatch => {
  dispatch(requestLoadboard())
  boardApi
    .loadBoard()
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.length >= 0) dispatch(receiveLoadboard(json))
      else if (json.err) dispatch(createboardError(json.err))
    })
    .catch(err => dispatch(loadboardError(err.message)))
}

//////////// LOAD DETAIL ////////////
function requestLoadDetail() {
  return {
    type: LOADDETAIL_REQUEST,
    isSuccess: false,
  }
}

function receiveLoadDetail(detail) {
  return {
    type: LOADDETAIL_SUCCESS,
    isSuccess: true,
    detail,
  }
}

function loadDetailError(message) {
  return {
    type: LOADDETAIL_FAILURE,
    isSuccess: false,
    message,
  }
}

export const loadDetail = id => dispatch => {
  dispatch(requestLoadDetail())
  boardApi
    .loadDetail(id)
    .then(response => response.json())
    .then(json => {
      if (json.err) dispatch(loadDetailError(json.err))
      else dispatch(receiveLoadDetail(json.lanes))
    })
    .catch(e => dispatch(loadDetailError(e.message)))
}

////////////////////////////////////////////////////////////////////////////////

function requestCreateList() {
  return {
    type: CREATELIST_REQUEST,
    isSuccess: false,
  }
}

function receiveCreateList(list) {
  return {
    type: CREATELIST_SUCCESS,
    isSuccess: true,
    list,
  }
}

function createListError(message) {
  return {
    type: CREATELIST_FAILURE,
    isSuccess: false,
    message,
  }
}

export const createList = (id, title) => dispatch => {
  dispatch(requestCreateList())
  boardApi
    .createList(title, id)
    .then(response => response.json())
    .then(json => {
      if (json.err) dispatch(createListError(json.err))
      else if (json.title) dispatch(receiveCreateList(json))
    })
    .catch(err => dispatch(createListError(err.message)))
}
////////////////////////////////////////////////////////////////////////////////

function requestCreateCard(listIndex) {
  return {
    type: CREATECARD_REQUEST,
    isSuccess: false,
    listIndex,
  }
}

function receiveCreateCard(card, listIndex) {
  return {
    type: CREATECARD_SUCCESS,
    isSuccess: true,
    card,
    listIndex,
  }
}

function createCardError(message) {
  return {
    type: CREATECARD_FAILURE,
    isSuccess: false,
    message,
  }
}

export const createCard = (listIndex, listId, title) => dispatch => {
  dispatch(requestCreateCard(listIndex))
  boardApi
    .createCard(listId, title)
    .then(response => response.json())
    .then(json => {
      if (json.err) dispatch(createCardError(json.err))
      else if (json.card) dispatch(receiveCreateCard(json.card, listIndex))
    })
    .catch(err => dispatch(createCardError(err.message)))
}

// ========================Add Due Date-Assigned : ThangND====================


function requestaddDuedate() {
  return {
    type: ADDDUEDATE_REQUEST,
    isSuccess: false,
  }
}

function receiveaddDuedate(dueDate) {
  return {
    type: ADDDUEDATE_SUCCESS,
    isSuccess: true,
    dueDate,
  }
}

function addDuedateError(message) {
  return {
    type: ADDDUEDATE_FAILURE,
    isSuccess: false,
    message,
  }
}
export const addDuedate = (cardId, dueDate)=>dispatch =>{
  dispatch(requestaddDuedate())
  boardApi
    .addDuedate(cardId,dueDate)
    .then(response => response.json())
    .then(json => {
      if (json.err) dispatch(addDuedateError(json.err))
        else if (json.dueDate) dispatch(receiveaddDuedate(json.dueDate))
    })
    .catch(err => dispatch(addDuedateError(err.message)))
}

function requestPriority(priority) {
  return {
    type: PRIORITY_REQUEST,
    isSuccess: false,
    card: {
      priority: priority,
    },
  }
}

function receivePriority(message) {
  return {
    type: PRIORITY_SUCCESS,
    isSuccess: true,
    message,
  }
}
function errorPriority(message) {
  return {
    type: PRIORITY_FAILURE,
    isSuccess: false,
    message,
  }
}
export const priority = priority => dispatch => {
  dispatch(receivePriority(priority))
  DetailAPI.priorityAPI(priority)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) dispatch(receivePriority(json.mess))
      else if (json.err) dispatch(errorPriority(json.err))
    })
    .catch(err => dispatch(errorPriority(err.message)))
}

//////////////////////////////////////////////

function requestMemberBoard(email) {
  return {
    type: ADDMEMBERBOARD_REQUEST,
    isSuccess: false,
    board: {
      email: email,
    },
  }
}

function receiveMemberBoard(message) {
  return {
    type: ADDMEMBERBOARD_SUCCESS,
    isSuccess: true,
    message,
  }
}
function errorMemberBoard(message) {
  return {
    type: ADDMEMBERBOARD_FAILURE,
    isSuccess: false,
    message,
  }
}

export const addBoardMember = email => dispatch => {
  dispatch(requestMemberBoard(email))
  DetailAPI.addMemberAPI(email)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) dispatch(receiveMemberBoard(json.mess))
      else if (json.err) dispatch(errorMemberBoard(json.err))
    })
    .catch(err => dispatch(errorMemberBoard(err.message)))
}

//========================================================================
function requestLoadCard() {
  return {
    type: LOADCARD_REQUEST,
    isSuccess: false,
  }
}

function receiveLoadCard(carddetail) {
  return {
    type: LOADCARD_SUCCESS,
    isSuccess: true,
    carddetail,
  }
}

function loadCardError(message) {
  return {
    type: LOADCARD_FAILURE,
    isSuccess: false,
    message,
  }
}

export const loadCard = (cardId) => dispatch => {
  dispatch(requestLoadCard())
  DetailAPI
    .loadCard(cardId)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.id) {console.log('Fucku'); dispatch(receiveLoadCard(json));}
      else if (json.err) dispatch(loadCardError(json.err))
    })
    .catch(err => dispatch(loadCardError(err.message)))
}