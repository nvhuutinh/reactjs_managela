import { REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE } from '../constants/ActionTypes'
import AuthAPI from '../utils/authApi'

function requestRegister(username, email, password, confirmPassword) {
  return {
    type: REGISTER_REQUEST,
    isAuthenticated: false,
    user: {
      username: username,
      email: email,
      password: password,
      confirmPassword: confirmPassword,
    },
  }
}

function receiveRegister(message) {
  return {
    type: REGISTER_SUCCESS,
    isAuthenticated: true,
    message,
  }
}

function registerError(error) {
  return {
    type: REGISTER_FAILURE,
    isAuthenticated: false,
    error,
  }
}

export const register = (username, email, password, confirmPassword) => dispatch => {
  dispatch(requestRegister(username, email, password, confirmPassword))
  AuthAPI.register(username, email, password, confirmPassword)
    .then(response => {
      return response.json()
    })
    .then(json => {
      if (json.mess) {
        dispatch(receiveRegister(json.mess))
      } else if (json.err) dispatch(registerError(json.err))
    })
    .catch(err => dispatch(registerError(err.message)))
}
