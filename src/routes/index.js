import React from 'react'
import { HomePage } from '../components'
import { Router, Route, Switch } from 'react-router-dom'
import styled from 'styled-components'
import {
  LoginContainer,
  RegisterContainer,
  ForgotContainer,
  UpdatePasswordContainer,
  BoardContainer,
  DetailContainer,
} from '../containers'
import history from './history'

const Container = styled.div`
  text-align: center;
`

function Routes() {
  return (
    <Router history={history}>
      <Container>
        <Switch>
          <Route exact path="/" component={HomePage} />
          <Route path="/login" component={LoginContainer} />
          <Route path="/register" component={RegisterContainer} />
          <Route path="/board" component={BoardContainer} />
          <Route path="/forgot" component={ForgotContainer} />
          <Route path="/detail/:id" component={DetailContainer} />
          <Route path="/updatepassword" component={UpdatePasswordContainer} />
        </Switch>
      </Container>
    </Router>
  )
}

export default Routes
