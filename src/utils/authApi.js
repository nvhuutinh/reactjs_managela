import host from '../config'
const AuthAPI = {
  login: (username, password) => {
    return fetch(host + '/login', {
      method: 'POST',
      body: JSON.stringify({ username, password }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  },
  register: (username, email, password, confirmPassword) => {
    return fetch(host + '/register', {
      method: 'POST',
      body: JSON.stringify({ username, email, password, confirmPassword }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  },

  forgot: email => {
    return fetch(host + '/forgotPassword', {
      method: 'POST',
      body: JSON.stringify({ email }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  },

  verifyMail: access_token => {
    return fetch(host + '/checktoken', {
      method: 'POST',
      body: JSON.stringify({ access_token }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  },

  updatePassword: (newPassword, confirmPassword, access_token) => {
    return fetch(host + '/updatePassword', {
      method: 'PUT',
      body: JSON.stringify({ newPassword, confirmPassword, access_token }),
      headers: {
        'Content-Type': 'application/json',
      },
    })
  },

  loadBoard: () => {
    return fetch(host + 'boards', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },

  createBoard: title => {
    return fetch(host + 'board', {
      method: 'POST',
      body: JSON.stringify({ title }),
      header: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
}
export default AuthAPI
