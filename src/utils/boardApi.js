import host from '../config'
const boardApi = {
  loadBoard: () => {
    return fetch(host + '/boards', {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
  loadDetail: id => {
    return fetch(host + '/board/' + id, {
      method: 'GET',
      headers: {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },

  createBoard: title => {
    return fetch(host + '/board', {
      method: 'POST',
      body: JSON.stringify({ title }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
  createCard: (listId, title) => {
    return fetch(`${host}/list/${listId}/card`, {
      method: 'POST',
      body: JSON.stringify({ title }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
  createList: (title, id) => {
    return fetch(host + '/board/' + id + '/list', {
      method: 'POST',
      body: JSON.stringify({ title }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
  addDuedate: (cardId, dueDate) => {
    return fetch(host+ '/cards/' +cardId, {
      method:'PUT',
      body: JSON.stringify({ dueDate }),
      headers:{
        'Content-Type':'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },

}
export default boardApi
