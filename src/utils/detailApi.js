import host from '../config'
const DetailAPI = {

  priorityAPI: (priority) => {                                
    return fetch(host + '/card/5a3378c59246defea6245cb3/priority', {
      method: 'PUT',
      body: JSON.stringify({ priority }),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },


  addMemberAPI: (email, boardID) => {
    return fetch(host + '/board/5a3378669246defea6245cb0/addMember', {
      method: 'POST',
      body: JSON.stringify({ email}),
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
 
  loadCard: (cardId) =>{
    return fetch(host + '/cards/'+ cardId, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      },
    })
  },
  
}
export default DetailAPI
