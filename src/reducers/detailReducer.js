import {
  LOADDETAIL_REQUEST,
  LOADDETAIL_SUCCESS,
  LOADDETAIL_FAILURE,

  CREATELIST_REQUEST,
  CREATELIST_FAILURE,
  CREATELIST_SUCCESS,

  CREATECARD_REQUEST,
  CREATECARD_FAILURE,
  CREATECARD_SUCCESS,

  ADDDUEDATE_REQUEST,
  ADDDUEDATE_SUCCESS,
  ADDDUEDATE_FAILURE,

  LOADCARD_REQUEST,
  LOADCARD_SUCCESS,
  LOADCARD_FAILURE,

} from '../constants/ActionTypes'

////////CREATE BOARD REDUCER////////////
export default function detailReducer(
  state = {
    isSuccess: false,
  },
  action
) {
  let newDetail = state.detail
  switch (action.type) {
    case LOADDETAIL_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
      })
    case LOADDETAIL_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        detail: action.detail,
      })
    case LOADDETAIL_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
      //////////////
    case CREATELIST_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
    case CREATELIST_SUCCESS:
      action.list.cards = []
      return Object.assign({}, state, {
        isSuccess: true,
        list: action.list,
        detail: [...newDetail, action.list],
      })
    case CREATELIST_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
      ////////////////
      case CREATECARD_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
        listIndex: action.listIndex,
      })
    case CREATECARD_SUCCESS:
      newDetail[action.listIndex].cards = [...newDetail[action.listIndex].cards, action.card]
      return Object.assign({}, state, {
        isSuccess: true,
        list: action.listIndex,
        card: action.card,
        detail: newDetail.slice(),
      })
    case CREATECARD_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })

    case ADDDUEDATE_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
    case ADDDUEDATE_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        list: action.list,
      })
    case ADDDUEDATE_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })

      // LOADCARD

    case LOADCARD_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })

    case LOADCARD_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        carddetail: action.carddetail,
      })

    case LOADCARD_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })

    default:
      return state
  }
}
