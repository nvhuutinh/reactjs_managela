import { combineReducers } from 'redux'
import authReducer from './authReducer'
import registerReducer from './registerReducer'
import forgotReducer from './forgotReducer'
import updatepasswordReducer from './updatepasswordReducer'
import detailReducer from './detailReducer'
import boardReducer from './boardReducer'

const rootReducer = combineReducers({
  authReducer,
  registerReducer,
  forgotReducer,
  updatepasswordReducer,
  detailReducer,
  boardReducer,
})

export default rootReducer
