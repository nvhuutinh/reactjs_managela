import { UPDATEPASSWORD_REQUEST, UPDATEPASSWORD_SUCCESS, UPDATEPASSWORD_FAILURE } from '../constants/ActionTypes'

export default function updatepasswordReducer(
  state = {
    isAuthenticated: localStorage.getItem('token') ? true : false,
  },
  action
) {
  switch (action.type) {
    case UPDATEPASSWORD_REQUEST:
      return Object.assign({}, state, {
        isAuthenticated: false,
        user: {
          newPassword: action.newPassword,
          confirmPassword: action.password,
        },
      })
    case UPDATEPASSWORD_SUCCESS:
      return Object.assign({}, state, {
        isAuthenticated: true,
        errorMessage: action.message,
      })
    case UPDATEPASSWORD_FAILURE:
      return Object.assign({}, state, {
        isAuthenticated: false,
        errorMessage: action.message,
      })

    default:
      return state
  }
}
