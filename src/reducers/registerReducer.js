import { REGISTER_REQUEST, REGISTER_SUCCESS, REGISTER_FAILURE } from '../constants/ActionTypes'

export default function registerReducer(
  state = {
    isAuthenticated: localStorage.getItem('token') ? true : false,
  },
  action
) {
  switch (action.type) {
    case REGISTER_REQUEST:
      return Object.assign({}, state, {
        user: {
          username: action.username,
          email: action.email,
          password: action.password,
          confirmPassword: action.confirmPassword,
        },
      })
    case REGISTER_SUCCESS:
      return Object.assign({}, state, {
        successMessage: action.message,
      })
    case REGISTER_FAILURE:
      return Object.assign({}, state, {
        errorMessage: action.error,
      })

    default:
      return state
  }
}
