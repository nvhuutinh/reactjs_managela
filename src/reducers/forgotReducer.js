import { FORGOT_REQUEST, FORGOT_SUCCESS, FORGOT_FAILURE } from '../constants/ActionTypes'

export default function forgotReducer(
  state = {
    isAuthenticated: localStorage.getItem('token') ? true : false,
  },
  action
) {
  switch (action.type) {
    case FORGOT_REQUEST:
      return Object.assign({}, state, {
        isAuthenticated: false,
        user: {
          email: action.email,
        },
      })
    case FORGOT_SUCCESS:
      return Object.assign({}, state, {
        isAuthenticated: true,
        errorMessage: action.message,
      })
    case FORGOT_FAILURE:
      return Object.assign({}, state, {
        isAuthenticated: false,
        errorMessage: action.message,
      })

    default:
      return state
  }
}
