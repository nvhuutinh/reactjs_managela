import {
  LOADBOARD_REQUEST,
  LOADBOARD_SUCCESS,
  LOADBOARD_FAILURE,
  CREATEBOARD_REQUEST,
  CREATEBOARD_SUCCESS,
  CREATEBOARD_FAILURE,
  ADDDUEDATE_REQUEST,
  ADDDUEDATE_SUCCESS,
  ADDDUEDATE_FAILURE,
} from '../constants/ActionTypes'

/////LOAD BOARD REDUCER//////////

export default function boardReducer(
  state = {
    isSuccess: false,
  },
  action
) {
  switch (action.type) {
    case LOADBOARD_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
      })
    case LOADBOARD_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        board: action.board,
      })
    case LOADBOARD_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })

    case CREATEBOARD_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
    case CREATEBOARD_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        board: [...state.board, action.board],
      })
    case CREATEBOARD_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
      
    case ADDDUEDATE_REQUEST:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
    case ADDDUEDATE_SUCCESS:
      return Object.assign({}, state, {
        isSuccess: true,
        list: action.list,
      })
    case ADDDUEDATE_FAILURE:
      return Object.assign({}, state, {
        isSuccess: false,
        errorMessage: action.message,
      })
    default:
      return state
  }
}
