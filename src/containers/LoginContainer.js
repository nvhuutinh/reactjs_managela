import React from 'react'
import { Login } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../actions/auth'

class LoginContainer extends React.Component {
  componentWillMount() {
    if (localStorage.getItem('token')) this.props.history.push('/board')
  }

  login = (username, password) => {
    this.props.login(username, password)
  }

  render() {
    return <Login login={this.login} message={this.props.message} />
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.authReducer.isAuthenticated,
    message: state.authReducer.errorMessage,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginContainer)
