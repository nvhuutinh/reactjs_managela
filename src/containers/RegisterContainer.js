import React from 'react'
import { Register } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../actions/register'

class RegisterContainer extends React.Component {
  componentWillMount() {
    if (localStorage.getItem('token')) this.props.history.push('/board')
  }

  register = (username, email, password, confirmPassword) => {
    this.props.register(username, email, password, confirmPassword)
  }

  render() {
    return (
      <Register
        register={this.register}
        errorMessage={this.props.errorMessage}
        successMessage={this.props.successMessage}
      />
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.registerReducer.isAuthenticated,
    successMessage: state.registerReducer.successMessage,
    errorMessage: state.registerReducer.errorMessage,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(RegisterContainer)
