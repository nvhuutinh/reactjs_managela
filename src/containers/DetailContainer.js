import React from 'react'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Detail } from '../components'
import * as boardActions from '../actions/board'


class DetailContainer extends React.Component {
  componentWillMount() {
    if (!localStorage.getItem('token')) this.props.history.push('/login')
    this.props.loadDetail(this.props.match.params.id)
  }
 
  componentWillReceiveProps(nextProps) {
    this.setState({ lanes: nextProps.lanes })
  }

  addDuedate = duedate=>{
    this.props.addDuedate(duedate)
  }

 priority = priority => {
    this.props.priority(priority)
    console.log(priority)

  }
 addBoardMember = email => {
    this.props.addBoardMember(email)
  }
  loadCard = cardId =>{
    this.props.loadCard(cardId)
  }

  render() {
    return (
      <Detail
        id={this.props.match.params.id}
        lanes={this.props.lanes}
        onAddLane={this.props.createList}
        onAddCard={this.props.createCard}
        priority={this.priority} message={this.props.message}
        addBoardMember={this.props.addBoardMember} message={this.props.message}
        addDuedate={this.props.addDuedate}
        loadCard={this.props.loadCard}
        card={this.props.card}
      />
    )
  }
}

const mapStateToProps = state => {
  console.log('12312312312312',state)
  return { 
    isSuccess: state.detailReducer.isSuccess,
    message: state.detailReducer.errorMessage,
    lanes: state.detailReducer.detail,
    card:state.detailReducer.carddetail,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(boardActions, dispatch)

}

export default connect(mapStateToProps, mapDispatchToProps)(DetailContainer)
