import React from 'react'
import { ShowBoard } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as boardActions from '../actions/board'

class BoardContainer extends React.Component {
  constructor(props) {
    super(props)
    this.props.loadBoard()
  }
  componentWillMount() {
    if (!localStorage.getItem('token')) this.props.history.push('/login')
  }
  createBoard = title => {
    this.props.createBoard(title)
  }

  render() {
    return (
      <div>
        <ShowBoard createBoard={this.createBoard} board={this.props.board}/>
      </div>
    )
  }
}

const mapStateToProps = state => {
  console.log(state)
  return {
    isSuccess: state.boardReducer.isSuccess,
    // message: state.boardReducer.errorMessage,
    board: state.boardReducer.board,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(boardActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(BoardContainer)
