import React from 'react'
import { ShowBoard } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class ShowboardContainer extends React.Component {
  componentWillMount() {
    if (!localStorage.getItem('token')) this.props.history.push('/login')
  }

  render() {
    return <ShowBoard />
  }
}

const mapStateToProps = state => {
  return {}
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({}, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ShowboardContainer)
