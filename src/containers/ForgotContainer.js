import React from 'react'
import { Forgot } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../actions/forgot'

class ForgotContainer extends React.Component {
  componentWillMount() {}

  verifyMail = access_token => {
    this.props.verifyMail(access_token)
  }

  forgot = email => {
    this.props.forgot(email)
  }

  render() {
    return (
      <div>
        <Forgot forgot={this.forgot} message={this.props.message} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.forgotReducer.isAuthenticated,
    message: state.forgotReducer.errorMessage,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotContainer)
