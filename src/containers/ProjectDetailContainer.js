import React from 'react'
import { Detail } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as detailActions from '../actions/detail'

class ProjectDetailContainer extends React.Component {
  priority = priority => {
    this.props.priority(priority)
    console.log(priority)

  }
 addBoardMember = email => {
    this.props.addBoardMember(email)
  }

  render() {
    return <Detail 
          priority={this.priority} message={this.props.message}
          addBoardMember={this.props.addBoardMember} message={this.props.message}
    />
  }
}

const mapStateToProps = state => {
  console.log(state)
  return {
    isSuccess: state.detailReducer.isSuccess,
    message: state.detailReducer.errorMessage,

  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(detailActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(ProjectDetailContainer)
