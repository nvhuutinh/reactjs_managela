import React from 'react'
import { UpdatePassword } from '../components'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import * as authActions from '../actions/forgot'

class UpdatePasswordContainer extends React.Component {
  componentWillMount() {}
  updatePassword = (newPassword, confirmPassword) => {
    const access_token = this.props.location.search.replace('?access_token=', '')
    this.props.updatePassword(newPassword, confirmPassword, access_token)
  }

  render() {
    return <UpdatePassword updatePassword={this.updatePassword} message={this.props.message} />
  }
}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.updatepasswordReducer.isAuthenticated,
    message: state.updatepasswordReducer.errorMessage || state.updatepasswordReducer.message,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(authActions, dispatch)
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePasswordContainer)
